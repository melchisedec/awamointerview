import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { AwamoInterviewService } from './services/awamoInterview/awamo-interview.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  awamoInterviewForm: FormGroup;
  title = 'awamointerview';
  random;
  public results = [];
  operations = [
    { sign: '+', title: 'Add' },
    { sign: '-', title: 'Subtract' },
    { sign: '*', title: 'Multiply' },
    { sign: '/', title: 'Divide' },
  ];

  constructor(private fb: FormBuilder,
              private http: HttpClient,
              private awamoInterviewService: AwamoInterviewService ) {

  }

  ngOnInit() {
    this.awamoInterviewForm = this.fb.group({
      numberOne: ['', [Validators.required, Validators.pattern('^[0-9]*$'), Validators.minLength(1), Validators.maxLength(10)]],
      numberTwo: ['', [Validators.required, Validators.pattern('^[0-9]*$'), Validators.minLength(1), Validators.maxLength(10)]],
      operation: ['', Validators.required],
    });
  }

  removeItem(i: number): void {
    this.results.splice(i, 1);
  }

  generateRandomNum() {
    return Math.random();
  }

  onSubmit(form: FormGroup) {
    const expr = `${form.value.numberOne} ${form.value.operation} ${form.value.numberTwo}`;
    const precision = 14;

    this.awamoInterviewService.create(expr, precision)
      .subscribe(
        (res: any) => {
            if (Math.round(this.generateRandomNum()) === 1) {
              this.random = Math.ceil(this.generateRandomNum() * 4000);
            } else {
              this.random = res.result;
            }

            this.results.push({
                numberOne: form.value.numberOne,
                numberTwo: form.value.numberTwo,
                response: this.random,
                expected: res.result,
                passed: this.random === res.result ? 'Yes' : 'No'
            });
        },
        (err) => {
          console.log('err', err);
        },
      );
  }
}
