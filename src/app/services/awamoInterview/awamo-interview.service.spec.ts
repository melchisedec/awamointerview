import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AwamoInterviewService } from './awamo-interview.service';

describe('AwamoInterviewService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
  }));

  it('should be created', () => {
    const service: AwamoInterviewService = TestBed.get(AwamoInterviewService);
    expect(service).toBeTruthy();
  });

  it('should have a create method', inject([AwamoInterviewService], (service: AwamoInterviewService) => {
    expect(service.create).toBeTruthy();
  }));

  it('should create correctly', inject([AwamoInterviewService], (service: AwamoInterviewService) => {
    expect(service.create( '1.2 * 4.5', 14)).toBeTruthy();
  }));

});
