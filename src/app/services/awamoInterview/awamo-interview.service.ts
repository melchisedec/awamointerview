import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AwamoInterviewService {
    private loginUrl = 'http://api.mathjs.org/v4/';

    constructor(private http: HttpClient) {}

    create(expr, precision) {
      const body = '{"expr":' + '"' + expr + '",' + '"precision":'  + precision + '}';
      const headers = new HttpHeaders();
      headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8');
      return this.http.post(this.loginUrl, body, { headers });
   }
}
